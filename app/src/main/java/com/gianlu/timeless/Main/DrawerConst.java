package com.gianlu.timeless.Main;

public class DrawerConst {
    public static final int HOME = 0;
    public static final int COMMITS = 1;
    public static final int PROJECTS = 2;
    public static final int LEADERS = 3;
    public static final int PREFERENCES = 4;
    public static final int SUPPORT = 5;
    public static final int DAILY_STATS = 6;
}
