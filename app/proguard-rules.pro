-dontwarn javax.xml.bind.DatatypeConverter
-dontwarn org.apache.commons.codec.digest.DigestUtils
-dontwarn org.apache.commons.codec.CharEncoding

-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception